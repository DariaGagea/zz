const express = require('express')
const bodyParser = require('body-parser')
const Sequelize = require('sequelize')

const mysql = require('mysql2/promise')

const DB_USERNAME = 'app'
const DB_PASSWORD = 'p@ss'

let conn

mysql.createConnection({
        user: DB_USERNAME,
        password: DB_PASSWORD
    })
    .then((connection) => {
        conn = connection
        return connection.query('CREATE DATABASE IF NOT EXISTS tw_exam')
    })
    .then(() => {
        return conn.end()
    })
    .catch((err) => {
        console.warn(err.stack)
    })

const sequelize = new Sequelize('tw_exam', DB_USERNAME, DB_PASSWORD, {
    dialect: 'mysql',
    logging: false
})

let Student = sequelize.define('student', {
    name: Sequelize.STRING,
    address: Sequelize.STRING,
    age: Sequelize.INTEGER
}, {
    timestamps: false
})


const app = express()
app.use(bodyParser.json())

app.get('/create', async(req, res) => {
    try {
        await sequelize.sync({ force: true })
        for (let i = 0; i < 10; i++) {
            let student = new Student({
                name: 'name ' + i,
                address: 'some address on ' + i + 'th street',
                age: 30 + i
            })
            await student.save()
        }
        res.status(201).json({ message: 'created' })
    }
    catch (err) {
        console.warn(err.stack)
        res.status(500).json({ message: 'server error' })
    }
})

app.get('/students', async(req, res) => {
    try {
        let students = await Student.findAll()
        res.status(200).json(students)
    }
    catch (err) {
        console.warn(err.stack)
        res.status(500).json({ message: 'server error' })
    }
})

/*
# Dată fiind aplicația `app` completați metoda `PUT` la adresa `/students/:id`:

- Dacă s-a trimis un request cu un corp gol sau nedifinit, se va returna un json cu următorul format: `{"message": "body is missing"}`. Codul de răspuns va fi: `400`; (0.5 pts)
- Dacă din corpul request-ului lipsesc proprietăți se va returna un json cu următorul format: `{"message": "malformed request"}`. Codul de răspuns va fi: `400`; (0.5 pts)
- Un student inexistent nu poate fi modificat. Dacă se cere modificarea unui student inexistent se va returna un mesaj cu formatul: `{"message": "not found"}`. Codul de răspuns va fi: `404`; (0.5 pts)
- Dacă studentul există și corpul request-ului este valid, va fi modificat și se va returna un răspuns cu  codul `202`. Corpul răspunsului va fi `{"message": "accepted"}`;(0.5 pts)
- Dacă se face un request `GET /students` corpul răspunsului trebuie să conțină 10 `students`, inclusiv cel modificat anterior; (0.5 pts)
*/

app.put('/students/:id', async(req, res) => {
    const id = req.params.id;
    if (req._body) {
        try {
            // TODO
            const student = await Student.findById(id);
            if (!student) {
                res.status(404).send({ message: 'not found' });
            }
            else {
                if (req.body.name && req.body.address && req.body.age) {
                    student.name = req.body.name;
                    student.address = req.body.address;
                    student.age = req.body.age;
                    res.status(202).json({ message: "accepted" });
                }
                else {
                    res.status(400).json({ message: "malformed request" });
                }
            }
        }
        catch (err) {
            console.warn(err.stack)
            res.status(500).json({ message: 'server error' })
        }
    }
    else {
        console.log(req.body);
        res.status(400).json({ message: 'body is missing' });
    }
})

module.exports = app
