const express = require('express')
const bodyParser = require('body-parser')


const app = express()
app.use(bodyParser.json())

app.locals.cars = [{
    brand : 'Ferrari',
    make : 'Testarosa',
    year : 1984
},{
    brand : 'Ferrari',
    make : '250 GT California',
    year : 1957
}]

/*
# Dată fiind aplicația `app` completați metodele `POST` și `GET` la adresa `/cars`:

- Dacă se face un request `GET /cars?filter=GT` ar trebui să se returneze o listă filtrată a msșinilor cu un cod de răspuns `200`; (0.5 pts)
- Dacă s-a trimis un request cu un corp gol sau nedefinit, se va returna un json cu următorul format: `{"message": "body is missing"}`. Codul de răspuns va fi: `400`; (0.5 pts)
- Dacă din corpul request-ului lipsesc proprietăți se va returna un json cu următorul format: `{"message": "malformed request"}`. Codul de răspuns va fi: `400`; (0.5 pts)
- `Year` trebuie să fie un număr mai mare ca 1860; în caz contrar se va returna un json cu următorul format: `{message: "year should be > 1860"}`. Codul de răspuns va fi: `400`; (0.5 pts)
- Dacă mașina trimisă prin corpul request-ului este validă, va fi adăugată și se va returna un răspuns cu  codul `201`. Corpul răspunsului va fi `{"message": "created"}`;(0.5 pts). 
- Dacă se face un request `GET /cars` corpul răspunsului trebuie să conțină 3 `car`, inclusiv cea adăugată anterior; (0.5 pts)
*/

app.get('/cars', async (req, res) => {
   // TODO
   if(req._body){
       if(req.body.brand && req.body.make && req.body.year){
           if(req.body.year<1860){
               res.status(400).json({message:"year should be > 1860"});
           }
           else{
               var car = req.body;
               app.locals.push(car);
               res.status(201).json({message:"created"});
           }
       }
       else{
           res.status(400).json({message:'malformed request'});
       }
   }
   else{
       res.status(400).json({message:"body is missing"})
   }
})

app.post('/cars', (req, res, next) => {
    var ok=0;
     if(req.body.constructor === Object && Object.keys(req.body).length === 0){
           res.status(500).send({message : 'Body is missing'}) 
       }else{
            if(req.body.hasOwnProperty('make')===true
           && req.body.hasOwnProperty('model')===true
           && req.body.hasOwnProperty('price')===true){
               if(req.body.price >0){
                   for(var i=0;i<app.locals.cars.length;i++){
                       if(app.locals.cars[i].model.localeCompare(req.body.model)===0){
                           ok=1;
                       }
                   }
                   if(ok===1){
                        res.status(500).send({message : 'Car already exists'}) 
                   } else{
                        app.locals.cars.push(req.body);
                        res.status(201).send({message: 'Created'})
                   }
               }else{
                    res.status(500).send({message : 'Price should be a positive number'}) 
               }
              
           } else{
               res.status(500).send({message : 'Invalid body format'}) 
           }
           
       }
       next();
   // res.status(244).json({message: 'Bad request'});
})

module.exports = app