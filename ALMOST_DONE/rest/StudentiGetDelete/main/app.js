const express = require('express')
const bodyParser = require('body-parser')
const Sequelize = require('sequelize')

const mysql = require('mysql2/promise')

const DB_USERNAME = 'app'
const DB_PASSWORD = 'p@ss'

let conn

mysql.createConnection({
    user : DB_USERNAME,
    password : DB_PASSWORD
})
.then((connection) => {
    conn = connection
    return connection.query('CREATE DATABASE IF NOT EXISTS tw_exam')
})
.then(() => {
    return conn.end()
})
.catch((err) => {
    console.warn(err.stack)
})

const sequelize = new Sequelize('tw_exam', DB_USERNAME, DB_PASSWORD,{
    dialect : 'mysql',
    logging: false
})

let Student = sequelize.define('student', {
    name : Sequelize.STRING,
    address : Sequelize.STRING,
    age : Sequelize.INTEGER
},{
    timestamps : false
})


const app = express()
app.use(bodyParser.json())

app.get('/create', async (req, res) => {
    try{
        await sequelize.sync({force : true})
        for (let i = 0; i < 10; i++){
            let student = new Student({
                name : 'name ' + i,
                address : 'some address on ' + i + 'th street',
                age : 30 + i
            })
            await student.save()
        }
        res.status(201).json({message : 'created'})
    }
    catch(err){
        console.warn(err.stack)
        res.status(500).json({message : 'server error'})
    }
})

/*
# Dată fiind aplicația `app` completați metoda `GET` la adresa `/students` și metoda `DELETE` la adresa `/students/:id`:

- Dacă se face un request `GET /students` ar trebui să se returneze o listă completă a studenților cu un cod de răspuns `200`; (0.5 pts)
- Dacă se face un request `GET /students?filter=etc` ar trebui să se returneze o listă filtrată a studenților cu un cod de răspuns `200`; (0.5 pts)
- Un student inexistent nu poate fi șters. Dacă se cere ștergerea unui student inexistent se va returna un mesaj cu formatul: `{"message": "not found"}`. Codul de răspuns va fi: `404`; (0.5 pts)
- Dacă studentul există, va fi șters și se va returna un răspuns cu  codul `202`. Corpul răspunsului va fi `{"message": "accepted"}`;(0.5 pts)
- Dacă se face un request `GET /students` corpul răspunsului trebuie să conțină 9 `students`, și să nu conțină studentul șters anterior; (0.5 pts)
*/

app.get('/students', async (req, res) => {
    try{
        // TODO
        await Student.findAll();
        res.status(200).json({message : 'created'})
        
    }
    catch(err){
        console.warn(err.stack)
        res.status(500).json({message : 'server error'})        
    }
})

app.delete('/students/:id', async (req, res) => {
    try{
        // TODO
        const id = req.params.id;
    	const student = await Student.findById(id);
    	if (!student) {
    		res.status(404).json({ message: 'not found' });
    	}
    	await student.destroy();
    	res.status(202).json({ message: 'accepted' });
    }
    catch(err){
        console.warn(err.stack)
        res.status(500).json({message : 'server error'})        
    }
})



module.exports = app