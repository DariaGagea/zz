/*
- Functia trebuie sa returneze un Promise; (0.5 pts)
- Daca `bonus` nu este numar, functia trebuie sa apeleze `reject` cu `Error` si mesajul `Invalid bonus`; (0.5 pts)
- `employees` este un vector ce contine elemente cu urmatorul format: `{name: string, salary: number}` (Example: [{name: "John Doe", salary: 5000}]). 
Daca este pasat un vector cu elemente invalide, functia trebuie sa apeleze `reject` cu `Error` si mesajul `Invalid array format`; (0.5 pts)
- Functia trebuie sa apeleze `reject` cu `string` cu valoarea `Bonus too small` daca `bonus` este mai mic de 10% din salariul maxim din `employees` 
array; (0.5 pts)
- Functia trebuie sa apeleze `resolve` cu un vector ce contine salariile marite pentru fiecare angajat; (0.5 pts) 
*/

function applyBonus(employees, bonus) {
    return new Promise((resolve, reject) => {
        if (bonus === null || isNaN(bonus)) {
            reject(Error('Invalid bonus'));
        }
        var max = 0;
        if (employees.filter(e => !(typeof e.name === "string" && typeof e.price === "number")).length) {
            reject(new Error("Invalid array format"));
        }
        for (let i = 0; i < employees.length; i++) {
            if (typeof(employees[i].name) === 'string' && typeof(employees[i].salary) === 'number') {
                if (employees[i].salary > max) {
                    max = employees[i].salary;
                }
            }
        }
        b=0.1 * max;
        if (bonus < b) {
            reject("Bonus too small");
        }
        for (let i = 0; i < employees.length; i++) {
            employees[i].salary = employees[i].salary + bonus;
        }
        resolve(employees);
    })
}

let app = {
    applyBonus: applyBonus,
}

module.exports = app;

/*
function applyBonus(employees, bonus){
 
    return new Promise((resolve, reject) => {
        if(typeof bonus != 'number'){
            reject(new Error('Invalid bonus'));
        }
        employees.forEach(element => {
            if(!element.hasOwnProperty('name')&&!element.hasOwnProperty('salary')){
                reject(new Error('Invalid array format'));
            }else{
                if(typeof element.name!=='string'&&typeof element.salary!=='number'){
                    reject(new Error('Invalid array format'));
                }
            }
        })
        let max = -999999999;
        for(let i=0; i< employees.length; i++){
            if (max < employees[i].salary){
                max =  employees[i].salary;
            }
        }
        let bon = 0.1 * max;
        if(bonus < bon){
            reject('Bonus too small');
        }
        else{
            resolve(employees.map(employee => {
                return{name: employee.name, salary: employee.salary+bonus};
            }))
        }
    })
    
}
*/