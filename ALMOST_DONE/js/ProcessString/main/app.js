/*
# Dată fiind funcția `function processString(input)`, care inițial tokenizează string-ul `input` în mai multe `tokens` separate de spațiu, rezolvați următoarele cerințe:

- Dacă oricare `token` nu este un `number` sau un `Number`, funcția ar trebui să arunce `Error` 
- Dacă oricare `token` nu este un `number` sau un `Number`, funcția ar trebui să arunce `Error` cu mesajul `Item is not a number`; (0.5 pts)
- Dacă `input` are lungime 0 funcția ar trebui să returneze 100; (0.5 pts)
- Token-urile `impare` sunt ignorate; (0.5 pts)
- Funcția returnează 100 minus suma tuturor `token`-urilor pare; (0.5 pts)
*/

function processString(input) {
    // TODO
    if (input.length === 0) {
        return 100;
    }
    let items = input.split(" ");
    let sum = 0;
    for (let i = 0; i < items.length; i++) {
        for (let j = 0; j < items[i].length; j++) {
            if (parseInt(items[i][j]) < 0 || parseInt(items[i][j]) > 9) {
                throw new Error("Item is not a number");
            }
        }
        // if (typeof(items[i]) === 'number') {
        //     // if (parseInt(items[i]) % 2 === 0) {
        //     //     sum += items[i];
        //     // }
        // }
    }
    return 100 - sum;
}

const app = {
    processString: processString
}

module.exports = app
