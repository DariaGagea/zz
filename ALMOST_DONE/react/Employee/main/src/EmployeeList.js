import React from 'react';
import AddEmployee from './AddEmployee.js';

export class EmployeeList extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            employees: []
        };
    }
    
    onAddMethod(employee){
        var newEmployees = this.state.employees;
        newEmployees.push(employee);
        this.setState({
           employees:newEmployees 
        });
    }

    render(){
        return(
            <div>
                <AddEmployee onAdd={this.onAddMethod}/>
                <div>{this.employees}</div>
            </div>
        )
    }
}