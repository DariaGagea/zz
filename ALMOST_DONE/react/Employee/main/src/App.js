import React, { Component } from 'react';
import EmployeeList from './EmployeeList.js';

class App extends Component {
  render() {
    return (
      <div>
        <EmployeeList/>
      </div>
    );
  }
}

export default App;
