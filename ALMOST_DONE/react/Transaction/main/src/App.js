import React from 'react';
import TransactionList from './TransactionList.js';

export default class App extends React.Component{

  render() {
    return (
      <div>
        <TransactionList />
      </div>
    );
  }
}
