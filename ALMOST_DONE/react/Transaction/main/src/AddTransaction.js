/*
# Having the following application created with `create-react-app` complete the following tasks:
- `AddTransaction` component should be rendered inside `TransactionList` component;
- `AddTransaction` component should contain 3 inputs with the following properties `id` and `name` having the following values: `transaction-number`, `transaction-type`, `transaction-amount`;
- `AddTransaction` component should contain an input of type `button` with the `value` property `add transaction`,used to trigger `addTransaction` method;
- `AddTransaction` component inside `TransactionList` should contain a `props` called `itemAdded`;
- When pressing `add transaction` button a new item should be displayed in `TransactionList` component; 
*/

import React from 'react';

export default class AddTransaction extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            transactionNumber: '',
            transactionType: '',
            amount: 0
        };
    }
    
    handleAdd = () => {
        let item = {...this.state};
        this.props.itemAdded(item);
    }

    render(){
        return (
        <div>
            <input id="transaction-number" name="transaction-number"></input>
            <input id="transaction-type" name="transaction-type"></input>
            <input id="transaction-amount" name="transaction-amount"></input>
            <input type="button" value="add transaction" onClick={this.handleAdd}></input>
        </div>
        );
    }
}