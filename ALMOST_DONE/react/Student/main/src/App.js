import React, { Component } from 'react';
import StudentList from './StudentList.js';

class App extends Component {
  render() {
    return (
      <div>
        <StudentList/>
      </div>
    );
  }
}

export default App;
