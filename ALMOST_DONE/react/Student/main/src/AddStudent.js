/*
# Avand urmatoarea apliatie create folosind `create-react-app`, completati urmatoarele taskuri:
- Componenta `AddStudent` trebuie adaugata in interiorul componentei `StudentList`;
- Componenta `AddStudent` trebuie sa contina 3 elemente de tip input cu `id-ul` si `name-ul`: `name`, `surname`, `age`;
- Componenta `AddStudent` trebuie sa contina un element input de tip buton `button` cu valoarea `add student`, folosit pentru a apela metoda `addStudent`;
- Componenta `AddStudent` din interiorul componentei `StudentList` trebuie sa contina in `props` metoda `onAdd`;
- La apasarea butonului `add student` un nou element trebuie afisat in componenta `StudentList`;
*/

import React, {Component} from 'react';

export class AddStudent extends Component {
    constructor(props){
        super(props);
        this.state = {
            name: '',
            surname: '',
            age: ''
        };
    }

    addStudent = () => {
        let student = {
            name: this.state.name,
            surname: this.state.surname,
            age: this.state.age
        };
        this.props.onAdd(student);
    }

    render(){
        return (
            <div>
                <input id="name" type="text" name="name"></input>
                <input id="surname" type="text" name="surname"></input>
                <input id="age" type="text" name="age"></input>
                <input type="button" value="add student" onClick={this.addStudent}></input>
            </div>
        )
    }
}