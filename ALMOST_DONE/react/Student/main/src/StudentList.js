import React, {Component} from 'react';
import AddStudent from './AddStudent.js'

export class StudentList extends Component {
    constructor(props){
        super(props);
        this.state = {
            students: []
        };
        this.props.addStudent;
    }
    
    onAddMethod(student){
        var newStudents=this.state.student;
        newStudents.push(student);
        this.setState({
           students:newStudents 
        });
    }

    render(){
        return (
            <div>
                <AddStudent onAdd={this.onAddMethod}/>
            </div>
        )
    }
}