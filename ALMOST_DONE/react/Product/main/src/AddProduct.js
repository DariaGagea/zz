/*
# Avand urmatoarea apliatie create folosind `create-react-app`, completati urmatoarele taskuri:
- Componenta `AddProduct` trebuie adaugata in interiorul componentei `ProductList`;
- Componenta `AddProduct` trebuie sa contina 3 elemente de tip input cu `id-ul` si `name-ul`: `name`, `category`, `price`;
- Componenta `AddProduct` trebuie sa contina un element input de tip buton `button` cu valoarea `add product`, folosit pentru a apela metoda `addProduct`;
- Componenta `AddProduct` din interiorul componentei `ProductList` trebuie sa contina in `props` metoda `onAdd`;
- La apasarea butonului `add product` un nou element trebuie afisat in componenta `ProductList`;
*/
 
import React from 'react';

export class AddProduct extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            name: '',
            category: '',
            price: ''
        };
    }

    addProduct = () => {
        let product = {
            name: this.state.name,
            category: this.state.category,
            price: this.state.price
        };
        this.props.onAdd(product);
    }

    render(){
        return (
            <div>
                <input id="name" name="name"></input>
                <input id="category" name="category"></input>
                <input id="price" name="price"></input>
                <input type="button" value="add product" onClick={this.addProduct}></input>
            </div>
        )
    }
}

export default AddProduct;