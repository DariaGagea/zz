/*
# Having the following application created with `create-react-app` complete the following tasks:
- `AddTask` component should be rendered inside `TaskList` component;
- `AddTask` component should contain 3 inputs with the following properties `id` and `name` having the following values: `task-name`, `task-priority`, `task-duration`;
- `AddTask` component should contain an input of type `button` with the `value` property `add task`, used to trigger `addTask` method;
- `AddTask` component inside `TaskList` should contain a `props` called `taskAdded`;
- When pressing `add task` button a new item should be displayed in `TaskList` component; 
*/

import React from 'react';

export default class AddTask extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            taskName: '',
            taskPriority: 'low',
            taskDuration: 0
        };
    }

    render(){
        return (
        <div>
            <input id = "task-name" name = "task-name"></input>
            <input id = "task-priority" name = "task-priority"></input>
            <input id = "task-duration" name = "task-duration"></input>
            <input type="button" value="add task" onClick = {this.addTask} ></input>
        </div>
        );
    }

    addTask = () => {
        let task = {...this.state};
        this.props.taskAdded(task);
    }
}