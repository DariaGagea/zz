/*
# Having the following application created with `create-react-app` complete the following tasks:
- `AddBook` component should be rendered inside `BookList` component;
- `AddBook` component should contain 3 inputs with the following properties `id` and `name` having the following values: `book-title`, `book-type`, `book-price`;
- `AddBook` component should contain an input of type `button` with the `value` property `add book`, used to trigger `handleAdd` method;
- `AddBook` component inside `BookList` should contain a `props` called `itemAdded `;
- When pressing `add book` button a new item should be displayed in `BookList` component; 
*/

import React from 'react';

export default class AddBook extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            bookTitle: '',
            bookType: '',
            bookPrice: 0
        };
    }
    
    onChange(target){
        this.setState({
            [target.name]:target.value
        });
    }

    render(){
        return (
        <div>
            <input id="book-title" name="book-title" onChange={this.onChange}></input>
            <input id="book-type" name="book-type" onChange={this.onChange}></input>
            <input id="book-price" name="book-price" onChange={this.onChange}></input>
            <input type="button" value = "add book" onClick={this.handleAdd}></input>
        </div>
        );
    }

    handleAdd = () => {
        console.log(this.state);
        let item = {
            bookTitle:this.state.bookTitle,
            bookType:this.state.bookType,
            bookPrice:this.state.bookPrice
        };
        if(typeof(item.bookTitle)==='string' && typeof(item.bookType)==='string' && typeof(item.bookPrice)==='number'){
             this.props.itemAdded(item);
        }
    }
}