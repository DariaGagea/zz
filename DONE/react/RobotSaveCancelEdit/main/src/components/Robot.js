import React, { Component } from 'react'

// TODO : adăugați posibilitatea de a edita un robot 
// editarea se face prin intermediul unui robot cu 2 stări, una de vizualizare și una de editare

class Robot extends Component {
	constructor(props) {
		super(props)
		this.state = {
			value: 'text'
		}
	}

	render() {
		let { item } = this.props
		return (
			<div>
				Hello, my name is {item.name}. I am a {item.type} and weigh {item.mass}
				{
					this.state.value==="edit" ? 
					
					(<div>
						<input id="name" name= "name" value = "test_name" type="text"></input>
						<input id="type" name= "type" value = "test_type" type="text"></input>
						<input id="mass" name= "mass" value = "test_mass" type="text"></input>
					
					<button value = "save" onClick={this.props.onSave}></button>
					<button value = "cancel" onClick={()=>{this.setState({value:"text"})}}></button>
					</div> )
					
					:
					null
					
				}
				<button value = "edit" onClick={()=>{this.setState({value:"edit"})}}></button>
			
			</div>
		)
	}
}

export default Robot
