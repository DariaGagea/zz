/*
# Having the following application created with `create-react-app` complete the following tasks:
- `AddVacation` component should be rendered inside `VacationList` component;
- `AddVacation` component should contain 3 inputs with the following properties `id` and `name` having the following values: `vacation-destination`, `vacation-location-type`, `vacation-price`;
- `AddVacation` component should contain an input of type `button` with the `value` property `add vacation`, used to trigger `addVacation` method;
- `AddVacation` component inside `VacationList` should contain a `props` called `itemAdded`;
- When pressing `add vacation` button a new item should be displayed in `VacationList` component; 
*/

import React from 'react';

export default class AddVacation extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            destination: '',
            locationType: '',
            price: 0
        };
    }

    handleAdd = () => {
        let item = {...this.state};
        this.props.itemAdded(item);
    }
    
    render(){
        return (
        <div>
            <input id = "vacation-destination" name="vacation-destination"></input>
            <input id = "vacation-location-type" name="vacation-location-type"></input>
            <input id = "vacation-price" name="vacation-price"></input>
            <input type="button" value = "add vacation" onClick={this.handleAdd}></input>
        </div>
        );
    }
}