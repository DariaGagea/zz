import React from 'react';
import AddVacation from './AddVacation.js';

export default class BookList extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            data: []
          };
    }
    
     onAddMethod = (v) => {
        const newV = this.state.data;
        newV.push(v);
        this.setState({
            data: newV
        });
    }

    render() {
        return (
            <div>
                <AddVacation itemAdded={this.onAddMethod}/>
            </div>
        );
    }
}