import React from 'react';
import VacationList from './VacationList.js';

export default class App extends React.Component{

  render() {
    return (
      <div>
        <VacationList />
      </div>
    );
  }
}
