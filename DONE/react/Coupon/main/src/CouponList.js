import React from 'react';
import AddCoupon from './AddCoupon.js';

export class CouponList extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            coupons: []
        };
    }

    onAddMethod(coupon){
        var newCoupons = this.state.coupons;
        newCoupons.push(coupon);
        this.setState({
            coupons:newCoupons
        });
    }

    render(){
        return(
            <div>
                <AddCoupon onAdd={this.onAddMethod} onChange = {console.log("abc")}/>
            </div>
        )
    }
}

export default CouponList;