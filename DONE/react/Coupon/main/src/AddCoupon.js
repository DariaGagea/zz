/*
- Componenta `AddCoupon` trebuie adaugata in interiorul componentei `CouponList`;
- Componenta `AddCoupon` trebuie sa contina 3 elemente de tip input cu `id-ul` si `name-ul`: `category`, `discount`, `availability`;
- Componenta `AddCoupon` trebuie sa contina un element input de tip buton `button` cu valoarea `add coupon`, folosit pentru a apela metoda `addCoupon`;

- Componenta `AddCoupon` din interiorul componentei `CouponList` trebuie sa contina in `props` metoda `onAdd`;
- La apasarea butonului `add coupon` un nou element trebuie afisat in componenta `CouponList`; */
import React from 'react';

export class AddCoupon extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            category: '',
            discount: '',
            availability: ''
        };
    }

    addCoupon = () => {
        let coupon = {
            category: this.state.category,
            discount: this.state.discount,
            availability: this.state.availability
        };
        this.props.onAdd(coupon);
    }
    
    componentDidMount(){
        this.props.onChange;
    }

    render(){
        return(
            <div>
                <input type="text" id="category" name="category"></input>
                <input type="text" id="discount" name="discount"></input>
                <input type="text" id="availability" name="availability"></input>
                <input type="button" value="add coupon" onClick={this.addCoupon}></input>
            </div>
        )
    }
}

export default AddCoupon;