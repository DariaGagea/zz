const express = require("express")

const fs=require("fs"); //fs - citire din fisiere
const bodyParser = require('body-parser');
const path = require('path');

const app = express()
app.use('/', express.static('public')); //pt a incarca lucruri din folderul public
app.use(bodyParser.json());

app.get("/", (req,res)=>{
   var data = fs.readFile("./public/article.json", function(err, data){
       if(err){
           throw err;
       }
       //creare html din node
       var newData = "<html><body><div>"+data+"</div></body></html>";
       fs.writeFile("./public/index.html", newData, function(err){
          if(err){
              throw err;
          } 
       });
       res.sendFile(path.join(__dirname + "/public/index.html"));
   }); 
});

app.listen(8080);

module.exports = app;