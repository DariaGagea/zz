/*
funcția calculateProduct primește ca parametru o listă de valori numerice și returnează produsul tuturor valorilor 
divizibile cu 3 mai mari ca 11 pentru o listă vidă funcția returnează 1
*/
function calculateProduct(array){
	var produsValDivCu3MMCa11=1;
	if(array!==undefined){
	    if(array.length>0){
	        for(let i=0;i<array.length;i++){
	            if(array[i]%3===0 && array[i]>11 && typeof(array[i])==='number'){
	                produsValDivCu3MMCa11*=array[i];
	            }
	            else if(typeof(array[i])!=='number'){
	                throw new Error("Conversion error");
	            }
	        }
	        return produsValDivCu3MMCa11;
	    }
	    else{
	        return 1;
	    }
	}
	else{
	    return 1;
	}
	
}


module.exports.calculateProduct = calculateProduct