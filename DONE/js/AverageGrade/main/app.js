function getAverageGrade(gradeItems) {
    // TODO
    var suma = 0;
    var noCount=0;
    if (gradeItems.length == 0) {
        return 0;
    }
    for (let i = 0; i < gradeItems.length; i++) {
        if (gradeItems[i].grade < 0) {
            throw new Error("Invalid grade");
        }
        if (typeof(gradeItems[i].grade) === 'number') {
            suma = suma + gradeItems[i].grade;
            noCount++;
        }
        if (gradeItems[i].grade !== 'A' && typeof(gradeItems[i].grade) !== 'number') {
            throw new Error("Invalid grade");
        }
    }
    var medie = suma / noCount;
    return medie;
}

const app = {
    getAverageGrade: getAverageGrade
}

module.exports = app