class Shape {
	constructor(dimensions) {
		this.dimensions = dimensions
	}
	area() {
		// TODO
		throw new Error("not implemented");
	}
}

// TODO: Square, Circle, Rectangle
class Square extends Shape {

	constructor(dimensions) {
		super(dimensions);
	}

	area() {
		return this.dimensions.width * this.dimensions.width;
	}
}

class Circle extends Shape {
	constructor(dimensions) {
		super(dimensions);
	}

	area() {
		return (Math.PI) * (Math.pow(this.dimensions.radius, 2));
	}
}

class Rectangle extends Shape {
	constructor(dimensions) {
		super(dimensions);
	}

	area() {
		return this.dimensions.width * this.dimensions.height;
	}
}

const app = {
	Shape: Shape,
	Square: Square,
	Circle: Circle,
	Rectangle: Rectangle
}

module.exports = app
