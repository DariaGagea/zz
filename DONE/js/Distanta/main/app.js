/*
 - funcția distance primește ca parametrii două array-uri
 - fiecare element poate apărea cel mult o dată într-un array; orice apariții suplimentare sunt ignorate 
 - distanța dintre cele 2 array-uri este numărul de elemente diferite dintre ele
 - dacă parametrii nu sunt array-uri se va arunca o excepție ("InvalidType")
*/
/*
 - the distance function receives as parameters two arrays
 - each element can appear in each array at most once; any duplicates are ignored
 - the distance between the 2 arrays is the number of different elements between them
 - if the parameters are not arrays an exception is thrown ("InvalidType")
*/

function distance(first, second) {
	//TODO: implementați funcția
	// TODO: implement the function

	if (Array.isArray(first) && Array.isArray(second)) {
		
		//facem set ca sa elimine duplicatele; [...] face la loc vector
		let primul = [...new Set(first)];
		let alDoilea = [...new Set(second)];
		
		let distanta = 0;
		
		//parcurgem vectorul si verificam daca elementele lui apartin celui de-al doilea
		for (let i = 0; i < primul.length; i++) {
			if (alDoilea.indexOf(primul[i]) === -1) { //alDoilea pe ce pozitie contine elemenul i din primul -> index of daca da -1 => elementul nu s-a gasit
				distanta++;
			}
			else {
				alDoilea.splice(alDoilea.indexOf(primul[i]), 1); //daca exista acel element in al doilea vector il elimin
			}
		} //dupa for in alDoilea o sa fie doar elemente unice
		distanta += alDoilea.length;
		return distanta;
	}
	else {
		throw new Error('InvalidType');
	}
}


module.exports.distance = distance
