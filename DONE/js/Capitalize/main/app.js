 /*
  - funcția capitalize primește ca parametrii un string și un array
  - dicționarul conține o serie de termeni
  - in textul ințial cuvintele sunt separate de spațiu
  - fiecare termen din dicționar trebuie să apară capitalizat în rezultat
  - rezultatul este un string nou, fără modificarea celui inițial
  - dacă textul nu este un string sau dicționarul nu este un array de string-uri se va arunca o excepție (mesajul TypeError)
 */

 function capitalize(text, dictionary) {
 	// TODO: implementați funcția
 	// TODO: implement the function
 	if (!Array.isArray(dictionary)) {
 		throw new Error("TypeError");
 	}
 	if (text !== null && dictionary !== null && typeof(text) === 'string') {
 		for (let i = 0; i < dictionary.length; i++) {
 			if (typeof(dictionary[i]) === 'string') {
 				text = text.replace(dictionary[i], dictionary[i][0].toUpperCase() + dictionary[i].slice(1, dictionary[i].length));
 				console.log(text);
 			}
 			else {
 				throw new Error("TypeError");
 			}
 		}
 		return text;
 	}
 	else {
 		throw new Error("TypeError");
 	}
 }


 module.exports.capitalize = capitalize
 