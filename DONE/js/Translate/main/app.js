/*
 - funcția translate primește ca parametrii un string și un obiect
 - funcția aruncă excepții dacă tipurile nu sunt respectate (mesajul "InvalidType")
 - obiectul dicționar are în cheie valoarea inițială și în valoare traducerea ei
 - valorile din dicționar sunt string-uri
 - funcția înlocuiește fiecare cheie din dicționar găsită în textul inițial cu valoarea tradusă
*/

function translate(text, dictionary) {
	if (typeof text !== 'string') {
		throw new Error('InvalidType');
	}
	if (typeof dictionary !== 'object' || !dictionary) {
		throw new Error('InvalidType');
	}
	for (let prop in dictionary) {
		if (typeof dictionary[prop] !== 'string') {
			throw new Error('InvalidType');
		}
	}
	let result = text.split(' ');
	for (let prop in dictionary) {
		let position = result.indexOf(prop); //indexOf arunca -1 daca nu gaseste
		if (position !== -1) {
			result[position] = dictionary[prop];
		}
	}
	return result.join(' ');
}



module.exports.translate = translate
