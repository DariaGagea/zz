/*
# Avand următoarea funcție `function toCamelCase(input)`, completați următoarele taskuri:

- Funcția trebuie să arunce un obiect `Error` cu mesajul `Input must be a string primitive or a string object` dacă inputul este un număr; (0.5 pts)
- Funcția trebuie să arunce un obiect `Error` cu mesajul `Input must be a string primitive or a string object` dacă inputul este un `undefined`; (0.5 pts)
- Dat fiind un input format din cuvinte separate de `-` funcția trebuie să îl returneze transformat în camelCase (cu inițiala cu literă mică); (0.5 pts)
- Dat fiind un input format din cuvinte separate de `_` funcția trebuie să îl returneze transformat în camelCase (cu inițiala cu literă mică); (0.5 pts)
- Dat fiind un input format din cuvinte separate de `spațiu` funcția trebuie să îl returneze transformat în camelCase (cu inițiala cu literă mică); (0.5 pts)
*/

function toCamelCase(input){
	if(typeof(input)==='number' || input===undefined){
	    throw new Error("Input must be a string primitive or a string object");
	}
	var items;
	if(input.includes("-")){
	    items=input.split("-");
	}
	if(input.includes("_")){
	    items=input.split("_");
	}
	if(input.includes(" ")){
	    items=input.split(" ");
	}
	var newText = items[0];
	for(let i=1;i<items.length;i++){
		newText=newText+items[i][0].toUpperCase()+items[i].slice(1,items[i].length);
	}
	return newText;
}

const app = {
  toCamelCase: toCamelCase
}

module.exports = app