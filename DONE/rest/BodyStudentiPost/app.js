const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');

const app = express();

app.use(bodyParser.json());
app.use(cors());

app.locals.students = [
    {
        name: "Gigel",
        surname: "Popel",
        age: 23
    },
    {
        name: "Gigescu",
        surname: "Ionel",
        age: 25
    }
];

app.get('/students', (req, res) => {
    res.status(200).json(app.locals.products);
});

/*
# Avand urmatoa aplicatie dezvoltata in NodeJS, sa se completeze metoda de tip `POST` de pe calea `/students` :

- Daca nu exista body pentru cererea http, trebuie sa returnati un JSON cu urmatorul format: `{message: "Body is missing"}`. Codul de raspuns trebuie sa fie: `500`;
- Daca body-ul nu respecta formatul unui student, trebuie sa returnati un JSON cu urmatorul format: `{message: "Invlid body format"}`. Codul de raspuns trebuie sa fie: `500`;
- Varsta unui student trebuie sa fie mai mare ca 0.In caz contrar trebuie sa returnati un JSON cu urmatorul format: `{message: "Age should be a positive number"}`. Codul de raspuns trebuie sa fie: `500`; 
- Daca studentul exista deja in vector, trebuie sa returnati un JSON cu urmatorul format: `{message: "Student already exists"}`.Codul de raspuns trebuie: `500`. Unicitatea se face in functie de nume;
- Daca body-ul are formatul corespunzator, studentul trebuie adaugat in vector si sa returnati un JSON cu urmatorul format: `{message: "Created"}`. Codul de raspuns trebuie sa fie: `201`;
*/
app.post('/students', (req, res, next) => {
     if(req.body.constructor===Object && Object.keys(req.body).length===0){
        res.status(500).json({message: 'Body is missing'});
    }
    try{
        var student =req.body;
        if(student.name && student.surname && student.age){
            if(student.age<0){
                res.status(500).json({message: 'Age should be a positive number'});
            }
            else{
                var ok = 1;
                for(var s of app.locals.students){
                    if(student.model===s.model){
                        ok=0;
                    }
                }
                if(ok==1){
                    app.locals.students.push(student);
                    res.status(201).send({message:'Created'});
                }
                else{
                    res.status(500).send({message:'Student already exists'});
                }
            }
        }
        else{
            res.status(500).json({message: 'Invalid body format'});
        }
    }
    catch(err){
        res.status(400).json({message: 'Bad request'});
    }
});

module.exports = app;