const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');

const app = express();

app.use(bodyParser.json());
app.use(cors());

app.locals.cars = [
    {
        make: "BMW",
        model: "X6",
        price: 50000
    },
    {
        make: "Lamborghini",
        model: "Huracan",
        price: 200000
    },
];

app.get('/cars', (req, res) => {
    res.status(200).json(app.locals.cars);
});

/*
# Avand urmatoa aplicatie dezvoltata in NodeJS, sa se completeze metoda de tip `POST` de pe calea `/cars` :

- Daca nu exista body pentru cererea http, trebuie sa returnati un JSON cu urmatorul format: `{message: "Body is missing"}`. Codul de raspuns trebuie sa fie: `500`;
- Daca body-ul nu respecta formatul unei masini, trebuie sa returnati un JSON cu urmatorul format: `{message: "Invlid body format"}`. Codul de raspuns trebuie sa fie: `500`;
- Pretul unei masini trebuie sa fie mai mare ca 0.In caz contrar trebuie sa returnati un JSON cu urmatorul format: `{message: "Price should be a positive number"}`. Codul de raspuns trebuie sa fie: `500`; 
- Daca masina exista deja in vector, trebuie sa returnati un JSON cu urmatorul format: `{message: "Product already exists"}`.Codul de raspuns trebuie: `500`. Unicitatea se face in functie de model;
- Daca body-ul are formatul corespunzator, masina trebuie adaugata in vector si sa returnati un JSON cu urmatorul format: `{message: "Created"}`. Codul de raspuns trebuie sa fie: `201`;
*/

app.post('/cars', (req, res, next) => {
    if(req.body.constructor===Object && Object.keys(req.body).length===0){
        res.status(500).json({message: 'Body is missing'});
    }
    try{
        var car =req.body;
        if(car.make && car.model && car.price){
            if(car.price<0){
                res.status(500).json({message: 'Price should be a positive number'});
            }
            else{
                var ok = 1;
                for(var c of app.locals.cars){
                    if(car.model===c.model){
                        ok=0;
                    }
                }
                if(ok==1){
                    app.locals.cars.push(car);
                    res.status(201).send({message:'Created'});
                }
                else{
                    res.status(500).send({message:'Car already exists'});
                }
            }
        }
        else{
            res.status(500).json({message: 'Invalid body format'});
        }
    }
    catch(err){
        res.status(400).json({message: 'Bad request'});
    }
});

module.exports = app;