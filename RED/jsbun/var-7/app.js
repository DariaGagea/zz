function applyDiscount(vehicles, discount){
     return new Promise((resolve, reject) => {
        if (typeof discount !== "number") {
            reject(Error("Invalid discount"));
        }
        else {
            let isArray = true;
            for (let obj of vehicles) {
                if (typeof obj.make !== "string" || typeof obj.price !== "number") {
                    isArray = false;
                    break;
                }
            }
            if (isArray === false) {
                reject(Error("Invalid array format"));
            }
            else {
                let priceArray = [];
                for (let obj of vehicles) {
                    priceArray.push(obj.price);
                }
                if (discount > Math.min(...priceArray) * 0.5) {
                    reject("Discount too big");
                }
                else{
                    resolve(vehicles.map(vehicle => {
                        let newPrice = vehicle.price-discount;
                        return { make: vehicle.make ,
                                 price: newPrice
                }
            }))
                 }
            }
        }
    })
}

const app = {
    applyDiscount: applyDiscount
};

module.exports = app;