// function applyBonus(employees, bonus){
//     return new Promise( (resolve, reject) =>{
//         if(typeof bonus !=="number"){
//             reject(Error("Invalid bonus"));
//         }else{
//             let isValidArray = true;
//             for(let obj of employees){
//                 if(typeof obj.name !== "string" || typeof obj.salary !== "number"){
//                     isValidArray = false;
//                     break;
//                 }
//             }
            
//             if(isValidArray == false){
//                 reject(Error("Invalid array format"));
//             }else {
//                 let salaryArray = [];
//                 for(let obj of employees){
//                     salaryArray.push(obj.salary);
//                 }
//                 if(bonus < Math.max(...salaryArray)*0.1){
//                     reject("Bonus too small");
//                 }else{
//                     let sal = []
//                     for(var i=0; i<employees.length;i++){
//                         employees[i].salary+= bonus;
//                         sal.push(employees[i].salary)
//                         console.log(employees[i].salary)
//                     }
//                     resolve(employees);
//                     console.log(Math.max(...sal));
//                     return Math.max(...sal)
//                 }
//             }
//         }
//     })
// }

// let app = {
//     applyBonus: applyBonus,
// }

// module.exports = app;

function applyBonus(employees, bonus){
    return new Promise( (resolve, reject) =>{
        if(typeof bonus !=="number"){
            reject(Error("Invalid bonus"));
        }else{
            let isValidArray = true;
            for(let obj of employees){
                if(typeof obj.name !== "string" || typeof obj.salary !== "number"){
                    isValidArray = false;
                    break;
                }
            }
            
            if(isValidArray == false){
                reject(Error("Invalid array format"));
            }else {
                let salaryArray = [];
                for(let obj of employees){
                    salaryArray.push(obj.salary);
                }
                if(bonus < Math.max(...salaryArray)*0.1){
                    reject("Bonus too small");
                }else{
                    resolve(employees.map(employee => {
                        let newSalary = employee.salary +bonus;
                        return { name: employee.name ,
                                 salary: newSalary
                }
            }))
                }
            }
        }
    })
}

let app = {
    applyBonus: applyBonus,
}

module.exports = app;